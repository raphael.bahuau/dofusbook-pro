# Dofusbook Pro

Some "pro" features for [Dofusbook](https://www.dofusbook.net/fr/)

## Features

- Add page title (stuff name, encyclopedia, workshop...)

### Effect Filter

- Improve effect filter interface to filter quickly by effect type (stats, dmg, res...)

### Workshop

- Copy to clipoard the resource name when click to quickly paste it on Dofus HDV interface
- Directly remove the resource when double click on the number input

## Changelog

- 1.5.0 Remove Exo/Over because of a website update + Improve page title + code refactoring
- 1.4.0 Page title pick the stuff name
- 1.3.0 Better effects filter + code refactoring
- 1.2.0 Add clipboard copy when click on resources in workshop
- 1.1.1 Add /public in exo/over feature to display also for other stuff than our
- 1.1.0 Workshop gesture, double clic on resources directly remove it
- 1.0.0 Exo/Over directly displayed over items in stuff
