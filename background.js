// Send url to content-script (inject.js)
function sendMessageUrl(tabId, url) {
  console.log("START " + tabId + " / " + url);
  chrome.tabs.sendMessage(tabId, { url });
}

// Detect url updates
chrome.webNavigation.onHistoryStateUpdated.addListener(
  (details) => {
    // Send the new url
    sendMessageUrl(details.tabId, details.url);
  },
  { urls: ["*://*.dofusbook.net/fr/*"] }
);

// Reload when click on extension button (need to refresh the page)
chrome.browserAction.onClicked.addListener(() => {
  chrome.runtime.reload();
});
