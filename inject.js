// TODO use functionnal loop (forEach, map, filter)
// TODO use more regex

// Utility : wait for an element on the DOM
function elementReady(selector, isAll) {
  return new Promise((resolve) => {
    // First check if it's already on the page
    const e = isAll
      ? document.querySelectorAll(selector)
      : document.querySelector(selector);
    if (isAll ? e.length : e) {
      resolve(e);
    }
    // Else set a mutation observer on the document
    new MutationObserver(function () {
      const e = isAll
        ? document.querySelectorAll(selector)
        : document.querySelector(selector);
      if (isAll ? e.length : e) {
        resolve(e);
        this.disconnect();
      }
    }).observe(document.documentElement, { childList: true, subtree: true });
  });
}

// Utility : classical sleep function
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

// Effect filter functionnalities
async function effectsFilter() {
  const eFilter = await elementReady(".filter-effect");
  let eBetterFilter = document.querySelector(".better-filter");
  // Return if already exist
  if (eBetterFilter) {
    return;
  }
  // Insert better filter
  eBetterFilter = document.createElement("div");
  eBetterFilter.setAttribute("class", "better-filter");
  eBetterFilter.innerHTML = `
        <div class="CmpTooltip c-search-tag" data-filter="^Do|^Dm">
            <span class="sprite-caracs carac-deg"></span>
            Dmg
        </div>

        <div class="CmpTooltip c-search-tag" data-filter="^Ré">
            <span class="sprite-caracs carac-res"></span>
            Res
        </div>

        <div class="CmpTooltip c-search-tag" data-filter="^Ret|^Esq|^Ta|^Fu">
            <span class="sprite-caracs carac-rall"></span>
            Ent /
            <span class="sprite-caracs carac-move"></span>
            Mob
        </div>
    `;
  eFilter.insertBefore(eBetterFilter, eFilter.firstElementChild);

  // Remove useless stats (doesn't remove weapon stats on weapon page)
  const regex = /context=weapon/.test(location.search)
    ? /JCJ/
    : /Dégâts|Vol|PV rendus|JCJ/;
  let eOptions = eFilter.querySelectorAll("option");
  for (const eOption of eOptions) {
    if (regex.test(eOption.innerText)) {
      eOption.parentElement.removeChild(eOption);
    }
  }
  // MAYBE use functionnal code ?
  // Array.from(eOptions).filter(eOption => regex.test(eOption.innerText))
  // Array.prototype.filter.call(eOptions, eOption => regex.test(eOption.innerText))
  // .forEach(eOption => eOption.parentElement.removeChild(eOption))

  const eFilterButtons = eFilter.querySelectorAll(".CmpTooltip");
  for (const eFilterButton of eFilterButtons) {
    eFilterButton.onclick = function () {
      // Only select once at a time
      const eOldSelected = eFilter.querySelector(".is-selected");
      if (eOldSelected && eOldSelected !== this) {
        eOldSelected.classList.remove("is-selected");
      }
      this.classList.toggle("is-selected");
      // Hide other stats
      const regex = new RegExp(this.dataset.filter);
      // Reset options list to avoid stats previously selected doesn't catched by system
      let eOptions = eFilter.querySelectorAll("option");
      for (const eOption of eOptions) {
        eOption.style.display = "";
        if (this.classList.contains("is-selected")) {
          if (!regex.test(eOption.innerText.trim())) {
            eOption.style.display = "none";
          }
        }
      }
    };
  }
}

// Workshop functionnalities
async function workshopClicks() {
  const eCells = await elementReady(".resources .cell", true);
  // On double click on input, fill the input with the max value
  const event = document.createEvent("HTMLEvents");
  event.initEvent("change", false, true);
  for (const eCell of eCells) {
    eCell.querySelector("input").ondblclick = function () {
      this.value = this.max;
      this.dispatchEvent(event);
    };
    // v1.0.3 Add clipboard copy on click on resources
    const eLabel = eCell.querySelector(".label");
    const copyLabel = () => {
      const eCopiedCell = document.querySelector(".resources .cell#copied");
      if (eCopiedCell) {
        eCopiedCell.removeAttribute("id");
      }
      navigator.clipboard.writeText(eLabel.textContent.trim()).then(() => {
        eLabel.parentElement.parentElement.setAttribute("id", "copied");
      });
    };
    eCell.querySelector(".img").onclick = copyLabel;
    eLabel.onclick = copyLabel;
  }
}

// Order of equipement type according to eTooltips variable
const HTML_ORDER = ["am", "br", "a1", "ce", "bo", "ch", "ar", "a2", "ca"];
// MAYBE 'fa', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6' ?
const ITEM_ORDER = ["a1", "a2", "am", "ar", "bo", "br", "ca", "ce", "ch"];
// MAYBE 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'fa ?
async function updateExover(pathname) {
  // Get the stuff informations
  const stuffId = pathname.split("/")[1].split("-")[0];
  const response = await fetch(
    `https://www.dofusbook.net/stuffs/dofus/public/${stuffId}`,
    {
      method: "GET",
      headers: {
        "accept-language": "fr",
        authorization: "Bearer " + localStorage.token,
      },
    }
  );
  const json = await response.json();
  console.log(json);

  // Get items cells
  const eTooltips = await elementReady(".stats-items .CmpTooltip", true);
  // Remove old exover display
  for (const eTooltip of eTooltips) {
    const eItem = eTooltip.querySelector(".item");
    eItem.dataset.exover = "";
  }
  // Set new exover
  for (const fmItemKey in json.fmItems) {
    const eTooltip = eTooltips[HTML_ORDER.indexOf(fmItemKey)];
    for (const stat in json.fmItems[fmItemKey]) {
      if (stat !== "pa" && stat !== "pm" && stat !== "po") {
        // Get item div
        const eItem = eTooltip.querySelector(".item");
        // Exo Invocation
        if (stat === "ic") {
          eItem.classList.add("exo", "exo-ic");
          return;
        }
        // Get text stat span
        const eText = eTooltip.querySelector(`.sprite-caracs.carac-${stat}`)
          .nextElementSibling;
        // Calculate base stat value
        let base = 0;
        if (!eText.parentElement.classList.contains("secondary-text")) {
          base = json.items[ITEM_ORDER.indexOf(fmItemKey)].effects.find(
            (effect) => effect.name === stat
          ).max;
        }
        const over = parseInt(json.fmItems[fmItemKey][stat]) - base;
        // Set data-exover value for item element to display on top of it's image
        eItem.dataset.exover = eText.textContent
          .replace(" Rés.", "")
          .replace("Dommages", "Do")
          .replace("Vitalité", "Vita")
          .replace("Invocation", "Invoc")
          .replace("Distance", "Dist")
          .replace("Critique", "Crit")
          .replace("Poussée", "Pou")
          .replace("Initiative", "Ini")
          // MAYBE PP, Stats (Intel, Puis?, Sasa?...), Esq PA/PM, Res Fixe ?
          .trim()
          .split(" ")
          .slice(1)
          .join(" ");
        eItem.dataset.exover = [over, eItem.dataset.exover]
          .join(" ")
          .replace(" %", "%");
        // If isn't an exo display original value + over value next to stat value
        if (!eText.parentElement.classList.contains("secondary-text")) {
          eText.dataset.exover = `(${base} + ${over})`;
        }
      }
    }
  }
}

// Stuff functionnalities
// TODO FM change stats order
// TODO FM change categories button order => Global, Par Item, Arme
// TODO add button in item card to go to fm
// TODO highlight exo/over stats in menu fm ?
// TODO lock exo PA/PM/PO checkbox if there is an item with explicit exo
// FIXME refresh exover when close FM dialog
async function stuffAndFm(pathname) {
  await updateExover(pathname);
}

// Set page title according to content
async function setPageTitle(pathname) {
  // Our stuff
  if (/^equipements.*/.test(pathname)) {
    const eInput = await elementReady(".input-title");
    document.title = "Stuff - " + eInput.value;
    eInput.addEventListener(
      "change",
      () => (document.title = "Stuff - " + eInput.value)
    );
  }
  // Other people stuff
  else if (/^equipement.*/.test(pathname)) {
    const eTitle = await elementReady(".title .u-ellipsis");
    document.title = "Stuff - " + eTitle.textContent.trim();
  }
  // Profil member pages or Single cloth
  else if (
    /(membre(.*)?\/profil)|(encyclopedie\/panoplie\/.+)/.test(pathname)
  ) {
    const eTitle = await elementReady("h3.title");
    document.title = eTitle.textContent.trim();
  }
  // Preferences
  else if (/membres\/preferences/.test(pathname)) {
    document.title = "Préférences";
  }
  // Home page
  else if (!pathname) {
    document.title = "Bienvenue sur Dofusbook !";
  }
  // Other page identified by active link on navbar
  else {
    const eLink = await elementReady(".router-link-exact-active");
    if (eLink) {
      // If it's an "Tous" page (equipements or dossiers)
      if (eLink.textContent.trim() === "Tous") {
        if (/equipements/.test(pathname)) {
          document.title = "Équipements";
        } else if (/dossiers/.test(pathname)) {
          document.title = "Dossiers";
        }
      }
      // If it's a single object page
      else if (/^encyclopedie\/objet/.test(pathname)) {
        document.title = eLink.textContent.trim();
      }
      // If from a navbar link
      const eCategorie =
        eLink.parentElement.parentElement.previousElementSibling;
      if (eCategorie.textContent.trim()) {
        document.title =
          eCategorie.textContent.trim() + " - " + eLink.textContent.trim();
      }
    }
  }
}

// Call function according to page
function start(pathname) {
  pathname = pathname.substr(4);
  setPageTitle(pathname);
  if (/encyclopedie/.test(pathname)) {
    effectsFilter();
  } else if (/atelier/.test(pathname)) {
    workshopClicks();
  } else if (/equipement(s?)/.test(pathname)) {
    stuffAndFm(pathname);
  }
}

// Receive message from background script
chrome.runtime.onMessage.addListener((r) => {
  if (r) {
    const url = new URL(r.url);
    start(url.pathname);
  }
});

start(location.pathname);
