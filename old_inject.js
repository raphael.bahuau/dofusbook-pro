function effectsFilter() {
    console.log('EF')
    // Select eCloak and Bag at same time
    // Doesn't work...
    /*
    new MutationObserver(function() {
        const eCloak = document.querySelector('img[alt=Cape]')
        if (eCloak) {
            this.disconnect()
            eCloak.onclick = () => {
                setTimeout(() => {
                    const params = new URL(location).searchParams
                    let category = params.get('category')
                    if (category) {
                        // Add Bag category
                        if (category.includes('ca') && !category.includes('sa')) {
                            category += '-sa'
                        }
                        // Remove Bag category
                        else if (!category.includes('ca') && category.includes('sa')) {
                            category = category.split('-').filter(c => c !== 'sa').join('-')
                        }
                        // Else do nothing and break the function to avoid update
                        else {
                            return
                        }
                        // Update url
                        params.set('category', category)
                        location.search = params.toString()
                    }
                }, 100)
            }
        }
    }).observe(document.body, { childList: true, subtree: true })
    */
    // Better filter
    new MutationObserver(function() {
        const eFilter = document.querySelector('.filter-effect')
        if (eFilter) {
            this.disconnect()
            let eBetterFilter = document.querySelector('.better-filter')
            if (!eBetterFilter) {
                // Insert better filter
                eBetterFilter = document.createElement('div')
                eBetterFilter.setAttribute('class', 'better-filter')
                eBetterFilter.innerHTML = `
                    <div class="CmpTooltip c-search-tag" data-filter="^D">
                        <span class="sprite-caracs carac-deg"></span>
                        Dmg
                    </div>

                    <div class="CmpTooltip c-search-tag" data-filter="Rés|Ren">
                        <span class="sprite-caracs carac-res"></span>
                        Res
                    </div>

                    <div class="CmpTooltip c-search-tag" data-filter="Ret|Esq|Ta|Fu">
                        <span class="sprite-caracs carac-rall"></span>
                        Ent /
                        <span class="sprite-caracs carac-move"></span>
                        Mob
                    </div>
                `
                eFilter.insertBefore(eBetterFilter, eFilter.firstElementChild.nextElementSibling)

                // Remove useless stats (doesn't remove weapon stats on weapon page)
                const regex = /context=weapon/.test(location.search) ? /JCJ/ : /Dégâts|Vol|PV rendus|JCJ/
                let eOptions = eFilter.querySelectorAll('option')
                for (const eOption of eOptions) {
                    if (regex.test(eOption.innerText)) {
                        // eOption.style.display = 'none'
                        eOption.parentElement.removeChild(eOption)
                    }
                }

                const eFilterButtons = eFilter.querySelectorAll('.CmpTooltip')
                for (const eFilterButton of eFilterButtons) {
                    eFilterButton.onclick = function() {
                        // Only select once at a time
                        const oldSelected = eFilter.querySelector('.is-selected')
                        if (oldSelected && oldSelected !== this) {
                            oldSelected.classList.remove('is-selected')
                        }
                        this.classList.toggle('is-selected')
                        // Hide other stats
                        const regex = new RegExp(this.dataset.filter)
                        for (const eOption of eOptions) {
                            eOption.style.display = ''
                            if (this.classList.contains('is-selected')) {
                                if (!regex.test(eOption.innerText.trim())) {
                                    eOption.style.display = 'none'
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }).observe(document.body, { childList: true, subtree: true })
}

function stuffExover() {
    // console.log('Dofusbook Pro : Stuff exo/over')
    const exovers = []

    // Get the stuff informations
    const stuff = location.pathname.split('/')[3].split('-')[0]
    // v1.0.2 : Add /public
    fetch(`https://www.dofusbook.net/api/stuffs/${stuff}/public`, {
        'method':'GET',
        'headers': {
            'accept-language': 'fr',
            'authorization': 'Bearer ' + localStorage.token,
        },
    }).then(response => response.json()).then(json => {
        // Update page title
        document.title = json.stuff.name

        // Find items with over or exo in the stuff (exept PA/PM/PO)
        for (const item of json.items) {
            for (const effect of item.effects) {
                if (!['pa', 'pm', 'po'].includes(effect.name) && 
                    (effect.is_exo || effect.fm > Math.max(effect.min, effect.max))) {
                    const value = (effect.is_exo) ? effect.fm : effect.fm - Math.max(effect.min, effect.max)
                    exovers.push({ id: item.id, carac: effect.name, value, isExo: effect.is_exo, base: effect.fm - value })
                }
            }
        }
        exovers.reverse()
        
        // Function to update exo/over display
        function updateExover() {
            // Wait for the html
            if (document.querySelector('.CmpTooltip-effect')) {
                console.log(json.stuff.name)
                // Reset display
                const eTooltips = document.querySelectorAll('.CmpTooltip-effects')
                for (const eTooltip of eTooltips) {
                    const eItem = eTooltip.parentElement.previousElementSibling
                    eItem.dataset.exover = ''
                    const eTexts = eTooltip.querySelectorAll('.CmpTooltip-effect')
                    for (const eText of eTexts) {
                        eText.dataset.exover = ''
                    }
                }
                // Add exo/over on new items
                for (const exover of exovers) {
                    // console.log(exover)
                    const eTooltip = document.querySelector(`.CmpTooltip-effects a[href="/fr/encyclopedie/objet/${exover.id}"]`).parentElement
                    const eItem = eTooltip.parentElement.parentElement.previousElementSibling
                    const eText = eTooltip.querySelector(`.sprite-caracs.carac-${exover.carac}`).nextElementSibling
                    let text = eText.textContent
                        .replace(' Rés.', '')
                        .replace('Dommages', 'Do')
                        .replace('Vitalité', 'Vita')
                        .replace('Invocation', 'Invoc')
                        .replace('Distance', 'Dist')
                        .replace('Critique', 'Crit')
                        .replace('Poussée', 'Pou')
                        .replace('Initiative', 'Ini')
                        // TODO PP, Stats (Intel, Puis?, Sasa?...), Esq PA/PM
                        .trim().split(' ').slice(1).join(' ')
                    text = [exover.value, text].join(' ')
                        .replace(' %', '%')
                    eItem.dataset.exover = text
                    if (!exover.isExo) {
                        eText.dataset.exover = `(${exover.base} + ${exover.value})`
                    }
                }
                return true
            }
            return false
        }

        if (!updateExover()) {
            // Then modify the CSS to display the over or exo value
            new MutationObserver(function() {
                if(updateExover()) {
                    this.disconnect()
                }
            }).observe(document.body, { childList: true, subtree: true })
        }
    })
}

function workshopClicks() {
    // console.log('Dofusbook Pro : Workshop')
    // Add dblclick event on resources input to quickly fill the input
    new MutationObserver(function() {
        // Wait for the html
        const cells = document.querySelectorAll('.resources .cell')
        if (cells.length) {
            this.disconnect()
            // On double click on input, fill the input with the max value
            const event = document.createEvent('HTMLEvents')
            event.initEvent('change', false, true)
            for (const cell of cells) {
                cell.querySelector('input').ondblclick = function() {
                    this.value = 9999
                    this.dispatchEvent(event)
                }
                // v1.0.3 Add clipboard copy on click on resources
                const label = cell.querySelector('.label')
                const copyLabel = () => {
                    const copied = document.querySelector('.resources .cell#copied')
                    if (copied) {
                        copied.removeAttribute('id')
                    }
                    navigator.clipboard.writeText(label.textContent.trim()).then(() => {
                        label.parentElement.parentElement.setAttribute('id', 'copied')
                    })
                }
                cell.querySelector('.img').onclick = copyLabel
                label.onclick = copyLabel
            }
        }
    }).observe(document.body, { childList: true, subtree: true })
}


function start(pathname) {
    // console.log(pathname)
    if (pathname === '/fr/encyclopedie/recherche') {
        effectsFilter()
    } else if (pathname === '/fr/outils/atelier') {
        workshopClicks()
    } else if (/\/fr\/equipement(s)?.*/.test(pathname)) {
        stuffExover()
    }
}

// Receive message from background script
chrome.runtime.onMessage.addListener(r => {
    if (r) {
        const url = new URL(r.url)
        start(url.pathname)
    }
})

start(location.pathname)